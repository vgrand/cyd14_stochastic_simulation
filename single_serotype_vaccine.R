
## model:
##  - the vaccine acts like a natural infection with one serotype (assumed 4)
##    (in this model world, no difference between which serotype)
##  - but has a potentially different transient immunity period
##  - only acts as "infection" if vaccinee would normally be infectable
##    (i.e., hasn't had s4 previously, no infections in last two years)

mechanism <- function(vpop, events, normal_transient_immune_period, mechargs) {
  transient_immune_period <- as.numeric(mechargs[1])
  full_prot_years <- as.integer(transient_immune_period)
  partial_prot_failure <- (transient_immune_period %% 1) # 1-(transient_immune_period %% 1)
  
  outcomes <- merge(
    events[ outcome != 4 ], # vaccine prevents all s4 exposures
    vpop, by = c("trial","id"), all = FALSE
  )[, keep := TRUE ]
  
  # for individuals that have not been exposed to s4
  #  or any other serotype recently
  outcomes[(s4 == FALSE) & ((lastinf <= -normal_transient_immune_period) | is.na(lastinf)),
    keep := (year > full_prot_years) # don't keep any exposures less than the full protection years
  ]
  
  # potentially protect against some exposures in partial protect years
  if (partial_prot_failure) {
    outcomes[(s4 == FALSE) & ((lastinf <= -normal_transient_immune_period) | is.na(lastinf)) & (year == (full_prot_years+1)),
      # assume attacks occur uniformly throughout the year
      # draw random attack times for all the events
      keep := (runif(.N) > partial_prot_failure)#,
      #by = .(trial, id)
    ]
  }
  
  attacks <- outcomes[keep == TRUE, {
    ev <- .SD[1, list(outcome, year)]
    lastyear <- ev$year
    infs <- ev$outcome
    while (.SD[(year >= (lastyear[1]+normal_transient_immune_period)) & !(outcome %in% infs), .N]) {
      last <- .SD[(year >= lastyear[1]+normal_transient_immune_period) & !(outcome %in% infs)][1, .(outcome, year)]
      lastyear <- c(last$year, lastyear)
      infs <- c(last$outcome, infs)
    }
    .(outcome = infs, year=lastyear)
  },by=.(trial, id)]
  
  result <- merge(
    attacks,
    vpop[,.(age, hr, ordinality=sum(s1,s2,s3,s4), seropositive=any(s1,s2,s3,s4)), by=.(trial,id)],
    on=c("trial","id"), all=FALSE
  )[, bump := FALSE ]
  
  bumpees <- outcomes[(s4 == FALSE) & ((lastinf <= -normal_transient_immune_period) | is.na(lastinf)), id, by=trial]
  
  result[bumpees, bump := TRUE, on = .(trial,id)]
  
  setkey(result, id, year)[, ordinality := ordinality[1]+(1L:.N)-1L+bump, keyby=.(trial,id)]  
  return(result)
}
# 
# require(data.table)
# 
# args <- commandArgs(trailingOnly=TRUE)
# # args <- c(
# #   paste0("~/Downloads/cyd14_trial_fitting_output/",
# #   c("vaccinepops.rds", "trial_events.rds", "coutcomes.rds")
# # ),"1.5")
# 
# vaccine_pops <- readRDS(args[1]) # consolidated vaccine populations
# vaccine_pops[, seropositive := any(s1,s2,s3,s4) ]
# trial_events <- readRDS(args[2])[ outcome != 4 ] # consolidated trial events, but immunize against serotype 4
# control_outcomes <- readRDS(args[3]) # consolidated trial events
# transient_immune_period <- as.numeric(args[4])
# normal_transient_immune_period <- 2
# 
# # for people *not* initially immune to s4 (either past exposure to s4 or other recent exposure),
# # provide some years of transient protection
# full_prot_years <- as.integer(transient_immune_period)
# 
# partial_prot_failure <- 1-(transient_immune_period %% 1)
# 
# measurements <- data.table()
# 
# # if (outcomes[(s1 & (outcome==1)) | (s2 & (outcome==2)) | (s3 & (outcome==3)) |(s4 & (outcome==4)),.N]) stop("exposures not properly filter on history")
# 
# for (rid in vaccine_pops[, unique(run)]) {
#   set.seed(rid)
#   vpop <- vaccine_pops[run == rid]
#   trial <- trial_events[run == rid]
#   control_ref <- control_outcomes[run == rid]
# 
#   result[, .N, keyby=.(trial, year, seropositive, age, risk=c("")[ordinality])]
#   control_ref[, .N, keyby=.(trial, year, seropositive, age, ordinality)]
# }
# 
# saveRDS(
#   result,
#   pipe("cat","wb")
# )