# usage: $(call first,middles...,last)
wrap = $(addsuffix $(3),$(addprefix $(1),$(2)))

# intent: refers to the invocation location
OUTPUTDIR = $(shell pwd)

# intent: refers to the Makefile location
SRCDIR = $(realpath $(dir $(realpath $(lastword $(MAKEFILE_LIST)))))/
#default: $(DBMAIN)/fig1.png
#default: supfigs mainfigs $(DBMAIN)/check_runs.png

srcref = $(addprefix $(SRCDIR),$(1))

FORECASTYEARS ?= 6

.PRECIOUS: populationCYD%.rds exposure_series_%.rds population_history_%.rds trial_events_%.rds coutcome_%.rds

prep: $(call srcref,assigned_populations.rds params.rds)

clean_prep:
	rm $(SRCDIR)*.rds

LOWER ?= 0
UPPER ?= 4

test: $(call wrap,coutcome_,$(shell seq -f '%03g' $(LOWER) $(UPPER)),.rds)

## overall process:
##  1. ingest data: estimate the required resolution, and format usefully
##  2. fit the annual exposure model
##  3. generate exposure series
##  4. using exposure series, generate trial population histories

## ------------------------------------------------------------------------------------------------- ##

## step 1, import some raw data and massage into useful format
## ingest the very sparse SP data on population (by age category)
## and estimate population (by age in years instead of age category)

$(SRCDIR)populationCYD%.rds: $(call srcref,pop_gen.R popdataCYD%.csv)
	cd $(SRCDIR) && Rscript $(notdir $^) > $(notdir $@)

# merge CYD14, CYD15
$(SRCDIR)populations.rds: $(call srcref,pop_merge.R populationCYD14.rds populationCYD15.rds)
	cd $(SRCDIR) && Rscript $(notdir $^) > $(notdir $@)

$(SRCDIR)assigned_populations.rds: $(call srcref,assignment.R populations.rds)
	cd $(SRCDIR) && Rscript $(notdir $^) > $(notdir $@)


## ------------------------------------------------------------------------------------------------- ##

## step 2, using the up-res population data + seroprevalence data for immunogenicity
## calculate the force of exposure, RR, proportion high-risk
## parameters for the heterogeneous risk model

$(SRCDIR)paramCYD%.rds: $(call srcref,ml_params.R populationCYD%.rds serodataCYD%.csv)
	cd $(SRCDIR) && Rscript $(notdir $^) > $(notdir $@)

# merge CYD14, CYD15 params
$(SRCDIR)params.rds: $(call srcref,param_merge.R paramCYD14.rds paramCYD15.rds)
	cd $(SRCDIR) && Rscript $(notdir $^) > $(notdir $@)

## ------------------------------------------------------------------------------------------------- ##

## step 3, generate exposure series
## CURRENTLY ASSUMES SEROTYPE MIXING PARAMETERS (PARS ?= ... line)
## these targets create samples for serotype exposure patterns

PARS ?= 5 0.02

exposure_series_%.rds: $(SRCDIR)exposure_series.R
	cd $(SRCDIR) && Rscript $(notdir $^) $(PARS) $(FORECASTYEARS) $(notdir $*) > $(OUTPUTDIR)/$(notdir $@)

exposure_series_test: $(call wrap,exposure_series_,$(shell seq -f '%03g' $(LOWER) $(UPPER)),.rds)

exposure_series.rds: $(SRCDIR)consolidate.R $(OUTPUTDIR)/exposure_series_*.rds
	Rscript $< $(OUTPUTDIR) exposure_series_ > $(OUTPUTDIR)/$(notdir $@)

exposure_check.png: $(SRCDIR)mixing_stat_plots.R exposure_series.rds
	Rscript $^ $@

## ------------------------------------------------------------------------------------------------- ##

## step 4, generate population histories
## read in the population files, create the appropriate # of participants by trial
## trial arms not yet designated at this stage, but membership in normal vs high risk
## is determined here.  Only does exposures up to trial start, then saves serostatus / last infection

population_history_%.rds: $(addprefix $(SRCDIR),population_history.R assigned_populations.rds params.rds) exposure_series_%.rds
	cd $(SRCDIR) && Rscript $(notdir $(filter $(SRCDIR)%,$^)) $(OUTPUTDIR)/$(filter-out $(SRCDIR)%,$^) $(notdir $*) > $(OUTPUTDIR)/$(notdir $@)

population_history_test: $(call wrap,population_history_,$(shell seq -f '%03g' $(LOWER) $(UPPER)),.rds)

population_histories.rds: $(SRCDIR)consolidate.R $(OUTPUTDIR)/population_history_*.rds
	Rscript $< $(OUTPUTDIR) population_history_ > $(OUTPUTDIR)/$(notdir $@)

# digest the runs into quantiles, prep for plotting pre-trial model world stats
pop_histories_plot.rds: $(SRCDIR)pop_hist_digest.R $(OUTPUTDIR)/population_histories.rds
	Rscript $^ > $(OUTPUTDIR)/$(notdir $@)

pop_histories_check.png: $(SRCDIR)pop_hist_check.R $(OUTPUTDIR)/population_histories.rds
	Rscript $^ $(OUTPUTDIR)/$(notdir $@)

population_history_clean:
	rm $(OUTPUTDIR)/population_history_*.rds
	rm $(OUTPUTDIR)/population_histories.rds
	rm $(OUTPUTDIR)/pop_histories_plot.rds

## ------------------------------------------------------------------------------------------------- ##
## INTERMEDIATE ANALYSIS
## assessing model against immunogenicity study control arm results

immuno_study_%.rds: $(call srcref,exposure_noise.R params.rds) exposure_series_*.rds population_history_*.rds
	Rscript $(wordlist 1,2,$^) exposure_series_ population_history_ 2 $(subst _,.,$(notdir $*)) > $@

series_imm_study: $(call wrap,immuno_study_,$(shell seq -f '1_%01g' $(LOWER) $(UPPER)),.rds)

## ------------------------------------------------------------------------------------------------- ##

## step 5, generate trial events
## using the exposure series + params, generate trial exposures,
## i.e., attacks for years 1-4

trial_events_%.rds: $(SRCDIR)trial_events.R population_history_%.rds exposure_series_%.rds $(SRCDIR)params.rds
	Rscript $^ $(notdir $*) > $(OUTPUTDIR)/$(notdir $@)

trial_events_test: $(call wrap,trial_events_,$(shell seq -f '%03g' $(LOWER) $(UPPER)),.rds)

trial_events.rds: $(SRCDIR)consolidate.R $(OUTPUTDIR)/trial_events_*.rds
	Rscript $< $(OUTPUTDIR) trial_events_ > $(OUTPUTDIR)/$(notdir $@)

assess_trial_events.png: $(SRCDIR)assess_trial_events.R $(OUTPUTDIR)/trial_events.rds
	Rscript $^ $(OUTPUTDIR)/$(notdir $@)

trial_events_clean:
	rm $(OUTPUTDIR)/trial_events_*.rds
	rm $(OUTPUTDIR)/trial_events.rds

## ------------------------------------------------------------------------------------------------- ##

# exposure_series_%.rds: $(SRCDIR)exposure_series.R
# 	Rscript $^ $(PARS) $(notdir $*) > $(OUTPUTDIR)/$(notdir $@)
#
# exposure_series_test: $(call wrap,exposure_series_,000 001 002 003 004 005 006 007 008 009,.rds)
#
# exposure_series.rds: $(SRCDIR)consolidate.R $(OUTPUTDIR)/exposure_series_*.rds
# 	Rscript $< $(OUTPUTDIR) exposure_series_ > $(OUTPUTDIR)/$(notdir $@)

## trial simulation concept:
##  - build basic population from known data (population.rds)
##  - assign the control and vaccine arms (assigned_populations.rds)
##  - generate a exposure series: the distribution of serotypes (exposure_series_%.rds)
##  - generate a population: random assignment of risk status + life history leading up to the trial (population_history_%.rds)
##  - generate trial exposure events: what exposures occur to which ids during trial period (trial_events_%.rds)
##  - apply the control model to control arm (control_outcome_%.rds)
##  - apply a vaccine model to vaccine arm? ($MODEL_outcome_%.rds)

# should be invoked:
#  - from $(outputtarget) directory, as
#  - make exposure_series_#INDEX.rds -f $(wherethislives)/Makefile PARS='serocycleperiodinyears fraccyclespeednoise'
#  - e.g., make exposure_series_000.rds -f trail_fitting/Makefile PARS='5 0.02'

# don't just make this - make exposure_ ... file first

coutcome_%.rds: $(SRCDIR)control.R population_history_%.rds trial_events_%.rds
	Rscript $^ > $(notdir $@)

$(OUTPUTDIR)/coutcomes.rds: coutcomes.rds

coutcomes.rds: $(SRCDIR)consolidate.R $(OUTPUTDIR)/coutcome_*.rds
	Rscript $< $(OUTPUTDIR) coutcome_ > $(OUTPUTDIR)/$(notdir $@)

$(OUTPUTDIR)/disease_risk.rds: disease_risk.rds

## TODO: determine disease risk fractions?
disease_risk.rds: $(SRCDIR)disease_risk.R $(OUTPUTDIR)/coutcome_*.rds
	Rscript $< $(OUTPUTDIR) population_history_ coutcome_ > $(OUTPUTDIR)/$(notdir $@)

# VACPARS ?= 0.9 0.5 4

# SSVACPARS ?= 1.5

# maoutcome_%.rds: $(SRCDIR)mavaccine.R population_history_%.rds trial_events_%.rds
# 	Rscript $^ $(notdir $*) $(VACPARS) > $(OUTPUTDIR)/$(notdir $@)

.PHONY: grid_ssoutcome grid_osoutcome

FILEROOTS = population_history_ trial_events_ coutcome_

grid_ssoutcome: $(SRCDIR)wrapper_vaccine.R $(SRCDIR)single_serotype_vaccine.R $(call wrap,$(OUTPUTDIR)/,$(FILEROOTS),*.rds) populations.rds disease_risk.rds
	Rscript $< $(OUTPUTDIR) $(FILEROOTS) \
	$(SRCDIR)populations.rds $(OUTPUTDIR)/disease_risk.rds $(SRCDIR)single_serotype_vaccine.R \
	$(SSVACPARS) > $(TEMPFILE)

series_ssoutcome: $(SRCDIR)wrapper_vaccine.R $(SRCDIR)single_serotype_vaccine.R $(call wrap,$(OUTPUTDIR)/,$(FILEROOTS),*.rds) populations.rds disease_risk.rds
	for x in 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3.0; do Rscript $< $(OUTPUTDIR) $(FILEROOTS) $(SRCDIR)populations.rds $(OUTPUTDIR)/disease_risk.rds $(SRCDIR)single_serotype_vaccine.R $$x; done;

grid_osoutcome: $(SRCDIR)wrapper_vaccine.R $(SRCDIR)other_serotype_vaccine.R  $(call wrap,$(OUTPUTDIR)/,$(FILEROOTS),*.rds) populations.rds disease_risk.rds
	Rscript $< $(OUTPUTDIR) $(FILEROOTS) $(SRCDIR)populations.rds $(OUTPUTDIR)/disease_risk.rds $(SRCDIR)single_serotype_vaccine.R $(SSVACPARS)


# reference_arm.rds: $(SRCDIR)reference.R coutcomes.rds controlpops.rds
# 	Rscript $^ > $(OUTPUTDIR)/$(notdir $@)
#
# ma_arm.rds: $(SRCDIR)reference.R maoutcomes.rds vaccinepops.rds
# 	Rscript $^ > $(OUTPUTDIR)/$(notdir $@)
#
# ma_RR.rds: $(SRCDIR)RR.R ma_arm.rds reference_arm.rds
# 	Rscript $^ > $(OUTPUTDIR)/$(notdir $@)
#
# control_plot.png: $(SRCDIR)control-plot.R reference_arm.rds
# 	Rscript $^ $(OUTPUTDIR)/$(notdir $@)
#
# ma_plot.png: $(SRCDIR)control-plot.R ma_arm.rds
# 	Rscript $^ $(OUTPUTDIR)/$(notdir $@)

## convenience consolidation targets for the sample steps

vaccine_parameters.rds: $(SRCDIR)vaccine_calcs.R $(addprefix $(OUTPUTDIR)/,population_histories.rds exposure_series.rds) $(addprefix $(SRCDIR),population.rds popdata.csv params.rds)
	Rscript $^ > $(OUTPUTDIR)/$(notdir $@)

# TODO some summary stats, graphics on exposure series

clean_exposure_series:
	rm $(OUTPUTDIR)/exposure_series*.rds

FIGURES = seropositive_comparisons.png population.png mixing.png

figures: $(FIGURES)

clean_figures:
	rm $(FIGURES)

seropositive_comparisons.png: $(addprefix $(SRCDIR),sero_plots.R populationCYD14.rds paramCYD14.rds serodataCYD14.csv populationCYD15.rds paramCYD15.rds serodataCYD15.csv)
	Rscript $^ $(OUTPUTDIR)/$(notdir $@)

population.png: $(addprefix $(SRCDIR),pop_plots.R populationCYD14.rds populationCYD15.rds popdataCYD14.csv popdataCYD15.csv)
	Rscript $^ $(OUTPUTDIR)/$(notdir $@)

# TODO some summary stats, graphics on population + params

.PHONY: mixing.png

mixing.png: $(SRCDIR)mixing_plots.R exposure_series.rds
	Rscript $^ $@

pre_trial_serostatus.png: $(SRCDIR)pophist_plots.R $(OUTPUTDIR)/pop_histories_plot.rds $(SRCDIR)serodataCYD14.csv $(SRCDIR)serodataCYD15.csv
	Rscript $^ $@

immunogenicity.png: $(SRCDIR)immunogenicity_plot.R $(SRCDIR)serodataCYD14.csv $(SRCDIR)serodataCYD15.csv $(SRCDIR)immunodata.csv $(OUTPUTDIR)/coutcome_*.rds
	Rscript $< $(SRCDIR)serodataCYD14.csv $(SRCDIR)serodataCYD15.csv $(SRCDIR)immunodata.csv $(OUTPUTDIR) coutcome_ $(OUTPUTDIR)/$(notdir $@)
