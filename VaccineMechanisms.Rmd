# Overview

We are considering a variety of dengue vaccine mechanisms, all of which operate within a general set of assumptions about the natural history of dengue infection and disease.  That natural model is that infections have risk associated with the ordinality of infection: 1st infections have mild risk, 2nd infections have high risk, and 3rd and 4th infections have low risk.  The empirical data indicates there are likely some asymmetries in disease risk by serotype infection order, but in our model, we are assuming infection order is irrelevant.  TODO: why do this relative to exposure model symmetry.

The trial results indicate the vaccine seems alter the expected ordinal relationship between natural infections and disease risk.  In our analysis, we consider vaccine mechanisms that have some probability of increasing a recipient's infection ordinality by 1 upon infectious exposure, but make no changes to the underlying disease risk model.  The alternative would be to consider alternative disease risk models for vaccinees, but this would further complicate identification of parameters as the control outcomes would no longer apply to the disease risk model.

## High Level Model Features

The next several models describe probabilities of protection or enhancement as a function of time since vaccination.  In addition to those trends, there are a two high level considerations for the models:

 - Is the effect identical (though not necessarily the outcome associated with that effect) for seronegative and seropositive recipients?
 - Is this vaccine effect durable or reset by an infection?

## Declining Protection / Antibody-Dependent Enhancement (ADE)

This model represents a vaccine mechanism that elicits a non-durable immune response.  As the response wanes, the probability for protection also wanes.  As protection wanes, the potential for enhanced disease (ADE) rises, until such time that immune response levels are too low for ADE.

To capture a simple waning model, we use exponential decay of protection.

$$
P\left(\mathrm{protection}\right) \propto \begin{cases}
  e^{-\lambda t} < \mathrm{lower\ threshold}:& 0\\
  e^{-\lambda t} < \mathrm{upper\ threshold}:& e^{-\lambda t}\\
  e^{-\lambda t} \geq \mathrm{upper\ threshold}:&1
\end{cases}
$$

and

$$
P\left(\mathrm{enhancement\ |\ \overline{protection}}\right) \propto \begin{cases}
  e^{-\lambda t} < \mathrm{lower\ threshold}:& 0\\
  e^{-\lambda t} < \mathrm{upper\ threshold}:& 1\
\end{cases}
$$

One way to parameterize the model is with a constant, $C$, upper ($u$) and lower ($l$) thresholds, and decay parameter $\lambda$.  In which case:

$$
P\left(\mathrm{protection}\right) = \begin{cases}
  Ce^{-\lambda t} < l:& 0\\
  l\leq Ce^{-\lambda t} < u:& {Ce^{-\lambda t}-l \over u-l}\\
  Ce^{-\lambda t} \geq u:&1
\end{cases}
$$

But there are two useful improvements to this model.  First, the entire curve allows y-axis scaling and transformation, so the $P(\mathrm{protection}) = Ce^{-\lambda t} - l$ is equivalent. Additionally, any one of $\{C,u,l\}$ may be fixed to particular value without loss of generality.  The obvious choice is $u=1$ - this is consistent with interpreting the curve as a probability.  This results:

$$
P\left(\mathrm{protection}\right) = \begin{cases}
  Ce^{-\lambda t} - l < 0:& 0\\
  Ce^{-\lambda t} - l < 1:& Ce^{-\lambda t}-l\\
  Ce^{-\lambda t} - l \geq 1:&1
\end{cases}
$$

The second useful improvement is to invert the parameters.  We described the model as mechanistically having threshold levels of some immune response, though there is no clear experimental measurement of said response.  We might just as easily think in terms of the timing of outcome phases - *i.e.* the time before which there are no cases in the vaccine arm ($t_{ux}$, time of upper threshold crossing) and the time after which there is no additional risk to being in the vaccine arm ($t_{lx}$, time of upper threshold crossing).  These values are direct, if statistical, measurements of the trial outcomes.

These times are defined by the equations

$$
1 = Ce^{-\lambda t_{ux}}-l \\
0 = Ce^{-\lambda t_{lx}}-l
$$

which can then be re-arranged to solve for $\{C,l\}$:

$$
C = {1 \over e^{-\lambda t_{ux}}-e^{-\lambda t_{lx}}} \\
l = {e^{-\lambda t_{lx}} \over e^{-\lambda t_{ux}}-e^{-\lambda t_{lx}}}
$$

and we can re-write our probability equation as:

$$
P\left(\mathrm{protection}\right) = \begin{cases}
  t \geq t_{lx}:& 0\\
  t_{ux} < t < t_{lx}:& {e^{\lambda(t_{lx}- t)} - 1 \over e^{\lambda(t_{lx}-t_{ux})}-1}\\
  t \leq t_{ux}:&1
\end{cases}
$$

One caveat to this formulation: we had $\lambda={\ln 2 \over T_{1/2}}$, the normal relationship between decay and half life, in the original formulation for the hypothetical immune response.  Due to the translation in the new expression, this relationship does not hold for the probability of protection.  This is modeling artefact, the only practical consequence being that we cannot use the relationships normally associated with $T_{1/2}$ for probability.  The actual $T_{1/2}$ for probability in this model is:

$$
t_{1/2}={\ln \frac{1}{2}\left(1-e^{-\lambda\left(t_{lx}-t_{ux}\right)}\right) \over -\lambda}
$$

which could be useful in the face of clear data about the particular feature, but is substantially more complicated to use in model calculations absent that data.

```{r, echo=FALSE, fig.show='asis'}
fx <- function(t, tux, tlx, Thalf,
  lambda=log(2)/Thalf, fac = exp(lambda*(tlx-tux))-1) {
  return((exp(-lambda*(t-tlx))-1)/fac)
}
tm <- seq(0, 4, .1); tux <- 0.5; tlx <- 2+(1/3); Thalf <- 1
plot(tm, fx(tm, tux, tlx, Thalf), type = "l", ylab="immune response curve", yaxt="n",
     main="Example Waning Protection & ADE Curve", xlab="years since first vaccine dose",
    ylim = c(0,1/(exp(-log(2)*tux/Thalf)-exp(-log(2)*tlx/Thalf)))-exp(-log(2)*tlx/Thalf)/(exp(-log(2)*tux/Thalf)-exp(-log(2)*tlx/Thalf))
)
abline(h=c(fx(c(tux, tlx), tux, tlx, Thalf),0.5), lty=2)
abline(v=c(tux, tux+Thalf, tlx), lty=2)
axis(1, labels = expression(t[ux],t[ux]+T[1/2],t[lx]), at = c(tux,tux+Thalf,tlx))
axis(2, labels = expression(C-l,1,0.5,0,-l), at = c(fx(0, tux, tlx, Thalf), 1, 0.5, 0, fx(Inf, tux, tlx, Thalf)), las=2)
text(c(3,1.5), c(0.75,0.75), c("no vaccine effect","protection\nor enhancement"))
text(0.25, 0.5, "protection", srt=90)
```

Given the trial outcomes, we know that $0\lt t_{ux} \leq 1$.  By construction, $t_{ux}\lt t_{lx}$ but the upper limit could plausibly exceed human lifetime.  However, the other examples of fading dengue ADE (*e.g.*, maternal antibodies) suggest that a few years is a plausible limit.

For $\lambda={\ln 2 \over T_{1/2}}$, we can imagine $\lambda \rightarrow 0$ (or $T_{1/2} \rightarrow \infty$).  However, with our construction, at a fixed $t_{lx}$, the ''curve'' still transitions at that time, and decay looks like a straight line between the limiting times.  Similarly, we can imagine $\lambda \rightarrow \infty$ (or $T_{1/2} \rightarrow 0$), in which case there is essentially a step function going from protection to enhancement at $t_{ux}$ and then swtiching to nothing at $t_{lx}$.

Given that the exponential models generalizes to both the linear and step function models in it's solution space, if the most likely parameterization of the data is not near either of those extremes, then it is not likely to be interesting to consider them.  However, if the solution space does prefer one of those extremes, then that alternative should be considered and subject to model comparisons.  Both of those models have one fewer parameters (corresponding to $\lambda$) than the exponential decay model.