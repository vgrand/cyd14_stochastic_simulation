suppressPackageStartupMessages(require(data.table))

args <- commandArgs(trailingOnly=TRUE)
# args <- c(
#  paste0("~/workspaces/cyd_test_outputs/", c("population_history_000.rds","exposure_series_000.rds")),
#  "~/workspaces/cyd14_stochastic_simulation/params.rds", "000"
# )

pophistory.dt <- readRDS(args[1])
exphistory.dt <- readRDS(args[2])
params <- readRDS(args[3])
set.seed(as.integer(args[4]))
pars <- data.table(trial = names(params), t(sapply(params, function(trial) unlist(trial$rrmodel$pars))))

# run the exposures for everyone

reference <- exphistory.dt[year>0]

outcomes <- paste0("s",1:4)
outcomesf <- factor(outcomes, levels=outcomes, ordered = T)

result <- pophistory.dt[pars, on="trial"][,{
  .(eachfoi=1-(1-ifelse(hr,foi*RR,foi))^(1/4), lastinf = ifelse(is.na(lastinf), -10L, lastinf), s1, s2, s3, s4)
}, keyby=.(trial, id)][,{
  ## generate attacks for each individual
  # note: minimal filtering at this stage - don't know what will happen w/ exposures
  # until trial is imposed (e.g., control vs vaccinated, which vaccine mechanism)
  immune <- which(c(s1, s2, s3, s4))
  reference[year >= (lastinf+2), .( # can filter by year (natural history model)
    year, s1, s2, s3, s4,
    attacks=rbinom(.N, 4, eachfoi)
  )][,.(
    outcome = setdiff(sample(4, size = attacks, replace = FALSE, prob=c(s1,s2,s3,s4)), immune) # can filter by previous immunity (natural history model)
  ), by=year]
},by=.(trial,id)]

# pophistory.dt[result, on="id"][(s1 & (outcome=="s1")) | (s2 & (outcome=="s2")) | (s3 & (outcome=="s3")) |(s4 & (outcome=="s4")), id]

saveRDS(result, pipe("cat","wb"))
